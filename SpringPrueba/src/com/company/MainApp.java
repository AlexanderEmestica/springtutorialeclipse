package com.company;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.company.dao.PersonasDao;
import com.company.pojo.Personas;

public class MainApp {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");

		PersonasDao personasDao = (PersonasDao) applicationContext.getBean("hexa");

		Personas personas = new Personas();

		personas.setNombre("Jhon");
		personas.setApellido("Dalton");
		personas.setTelefono(89651478);
		personas.setDinero(89.23);

		if (personasDao.save(personas)) {
			System.out.println("Guardado Correctamente");
		} else {
			System.out.println("Error al Momento de Guardar");
		}

		((ClassPathXmlApplicationContext) applicationContext).close();

	}

}
