package com.company.pojo;

public class Personas {

	private int id_personas;
	private String nombre;
	private String apellido;
	private int telefono;
	private Double dinero;

	public int getId_personas() {
		return id_personas;
	}

	public void setId_personas(int id_personas) {
		this.id_personas = id_personas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public Double getDinero() {
		return dinero;
	}

	public void setDinero(Double dinero) {
		this.dinero = dinero;
	}

	@Override
	public String toString() {
		return "Administrador [id_personas=" + id_personas + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", telefono=" + telefono + ", dinero=" + dinero + "]";
	}

}
