package com.company.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Direcciones {
	
	private String calle;
	private String cp;
	
	
	public Direcciones() {
		
	}
	
	public Direcciones(String calle, String cp) {
		this.calle = calle;
		this.cp = cp;
	}

	@Autowired
	public void setCalle(@Value("Heroes")String calle) {
		this.calle = calle;
	}

	@Autowired
	public void setCp(@Value("111")String cp) {
		this.cp = cp;
	}

	@Override
	public String toString() {
		return "Direcciones [calle=" + calle + ", cp=" + cp + "]";
	}
}
