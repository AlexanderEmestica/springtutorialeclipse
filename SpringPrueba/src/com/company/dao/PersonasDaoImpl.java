package com.company.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.company.pojo.Personas;

@Component("hexa")
public class PersonasDaoImpl implements PersonasDao {
	
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	private void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public boolean save(Personas personas) {
		
		MapSqlParameterSource paramMap = new MapSqlParameterSource();
		
		paramMap.addValue("nombre", personas.getNombre());
		paramMap.addValue("apellido", personas.getApellido());
		paramMap.addValue("telefono", personas.getTelefono());
		paramMap.addValue("dinero", personas.getDinero());
		
		return jdbcTemplate.update("insert into personas (nombre, apellido, telefono, dinero) values (:nombre, :apellido, :telefono, :dinero)", paramMap) == 1;
	}
	
	

}
